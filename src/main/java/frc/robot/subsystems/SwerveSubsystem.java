package frc.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;
import com.swervedrivespecialties.swervelib.Mk4SwerveModuleHelper;
import com.swervedrivespecialties.swervelib.SwerveModule;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Creates a subsystem to control a swerve drive base with xbox controllers
 *
 * @author Phillip Lai
 * @author Matthew Mcneilly
 */
public class SwerveSubsystem extends SubsystemBase {
    private final SwerveModule leftFrontModule;
    private final SwerveModule leftRearModule;
    private final SwerveModule rightFrontModule;
    private final SwerveModule rightRearModule;

    private SlewRateLimiter yLimiter;
    private SlewRateLimiter xLimiter;
    private SlewRateLimiter turningLimiter;

    ShuffleboardTab tab = Shuffleboard.getTab("Drivetrain");
    // adds values to a tab in smart dashboard

    private ChassisSpeeds chassisSpeeds = new ChassisSpeeds(0.0, 0.0, 0.0);

    public SwerveSubsystem() {
        this.xLimiter = new SlewRateLimiter(Constants.Swerve.MAX_ACCELERATION_UNITS_PER_SEC);
        this.yLimiter = new SlewRateLimiter(Constants.Swerve.MAX_ACCELERATION_UNITS_PER_SEC);
        this.turningLimiter =
                new SlewRateLimiter(Constants.Swerve.MAX_ANGULAR_ACCELERATION_UNITS_PER_SEC);

        /*
         * How to set offsets
         * 1. Set all the wheels to be facing forward
         * 2. Set all of the offsets to 0 and deploy
         * 3. Read the 'Current Angle' value off of smart dashboard and make that number the modules offset
         * 4. Deploy the code and wait for errors
         */
        leftFrontModule =
                Mk4SwerveModuleHelper.createFalcon500(
                        tab.getLayout("Left Front Module", BuiltInLayouts.kList)
                                .withSize(2, 4)
                                .withPosition(0, 0),
                        Mk4SwerveModuleHelper.GearRatio.L3,
                        Constants.Drive.LEFT_FRONT_DRIVE_PORT,
                        Constants.Drive.LEFT_FRONT_TURNING_PORT,
                        Constants.Drive.LEFT_FRONT_CAN_CODER_PORT,
                        Constants.Drive.LEFT_FRONT_TURNING_OFFSET);

        leftRearModule =
                Mk4SwerveModuleHelper.createFalcon500(
                        tab.getLayout("Left Rear Module", BuiltInLayouts.kList)
                                .withSize(2, 4)
                                .withPosition(2, 0),
                        Mk4SwerveModuleHelper.GearRatio.L3,
                        Constants.Drive.LEFT_REAR_DRIVE_PORT,
                        Constants.Drive.LEFT_REAR_TURNING_PORT,
                        Constants.Drive.LEFT_REAR_CAN_CODER_PORT,
                        Constants.Drive.LEFT_REAR_TURNING_OFFSET);

        rightFrontModule =
                Mk4SwerveModuleHelper.createFalcon500(
                        tab.getLayout("Right Front Module", BuiltInLayouts.kList)
                                .withSize(2, 4)
                                .withPosition(4, 0),
                        Mk4SwerveModuleHelper.GearRatio.L3,
                        Constants.Drive.RIGHT_FRONT_DRIVE_PORT,
                        Constants.Drive.RIGHT_FRONT_TURNING_PORT,
                        Constants.Drive.RIGHT_FRONT_CAN_CODER_PORT,
                        Constants.Drive.RIGHT_FRONT_TURNING_OFFSET);

        rightRearModule =
                Mk4SwerveModuleHelper.createFalcon500(
                        tab.getLayout("Right Rear Module", BuiltInLayouts.kList)
                                .withSize(2, 4)
                                .withPosition(6, 0),
                        Mk4SwerveModuleHelper.GearRatio.L3,
                        Constants.Drive.RIGHT_REAR_DRIVE_PORT,
                        Constants.Drive.RIGHT_REAR_TURNING_PORT,
                        Constants.Drive.RIGHT_REAR_CAN_CODER_PORT,
                        Constants.Drive.RIGHT_REAR_TURNING_OFFSET);
    }

    public void setSpeeds(ChassisSpeeds chassisSpeeds) {
        this.chassisSpeeds = chassisSpeeds;
    }

    public void setSpeeds(
            double xSpeed, double ySpeed, double rotationSpeed, boolean robotOriented) {
        if (robotOriented) {
            chassisSpeeds = new ChassisSpeeds(-xSpeed, -ySpeed, -rotationSpeed);
        } else {
            chassisSpeeds =
                    ChassisSpeeds.fromFieldRelativeSpeeds(
                            xSpeed, ySpeed, -rotationSpeed, getRotation2d());
        }
    }

    private final AHRS gyro = new AHRS(SPI.Port.kMXP, (byte) 200);

    /**
     * call this repeatedly until you need it to stop. replace this with new version when new
     * version works.
     *
     * @return true if it is "balanced"
     */
    public boolean balancing() {
        double turningSpeed = 0.0, ySpeed = 0.0;
        boolean returnval;
        // if (getRoll() > 5) {
        //     turningSpeed = -Math.PI;
        //     returnval = false;
        // } else if (getRoll() < -5) {
        //     turningSpeed = Math.PI;
        //     returnval = false;}
        if (getPitch() > -10) {
            ySpeed = -0.75;
            returnval = false;
        } else if (getPitch() < 10) {
            ySpeed = 0.75;
            returnval = false;
        } else {
            ySpeed = 0;
            returnval = true;
        }
        setSpeeds(0, yLimiter.calculate(ySpeed), turningLimiter.calculate(turningSpeed), true);
        return returnval;
    }

    public double getPitch() {
        return gyro.getPitch();
    }

    public Rotation2d getRotation2d() {
        return Rotation2d.fromDegrees(-gyro.getYaw() - 180);
    }

    public void stopModules() {
        chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(0, 0, 0, new Rotation2d(0));
    }

    public void zeroGyro() {
        gyro.zeroYaw();
    }

    @Override
    public void periodic() {
        SwerveModuleState[] states =
                Constants.Drive.SWERVE_DRIVE_KINEMATICS.toSwerveModuleStates(chassisSpeeds);

        SmartDashboard.putNumber("Robot Heading", gyro.getYaw());
        SmartDashboard.putNumber(
                "Robot Speed",
                (states[0].speedMetersPerSecond
                                + states[1].speedMetersPerSecond
                                + states[2].speedMetersPerSecond
                                + states[3].speedMetersPerSecond)
                        / 4);

        SwerveDriveKinematics.desaturateWheelSpeeds(states, Constants.Swerve.SPEED);

        leftFrontModule.set(states[0].speedMetersPerSecond, states[0].angle.getRadians());
        leftRearModule.set(states[1].speedMetersPerSecond, states[1].angle.getRadians());
        rightFrontModule.set(states[2].speedMetersPerSecond, states[2].angle.getRadians());
        rightRearModule.set(states[3].speedMetersPerSecond, states[3].angle.getRadians());
    }
}
