// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Creates a new ArmSubsytem. designed to change the angle of the arm
 *
 * @author Kyle Briggs
 * @author Phil Lai
 */
public class AngleSubsystem extends SubsystemBase {
    private static AngleSubsystem instance;
    private TalonFX angleMotor;
    private Angle changeAngle;
    private CANCoder absoluteEncoder = new CANCoder(Constants.Angle.CANCODER_ID);
    private DoubleSolenoid brakeSolenoid =
            new DoubleSolenoid(
                    2,
                    PneumaticsModuleType.REVPH,
                    Constants.Angle.BRAKE_OFF_PORT /*turns brake off*/,
                    Constants.Angle.BRAKE_ON_PORT /*turns brake on */);

    /** Sets if the arm is changing its angle and which direction */
    public enum Angle {
        ConeHighTarget,
        ConeMiddleTarget,
        CubeHighTarget,
        CubeMiddleTarget,
        Substation,
        Floor,
        Down,
        Stop
    }

    /**
     * @return the {@link AngleSubsystem} singleton
     */
    public static AngleSubsystem getInstance() {
        if (instance == null) {
            instance = new AngleSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    public AngleSubsystem() {
        changeAngle = Angle.Stop;
        brakeSolenoid.set(Value.kReverse);

        angleMotor = new TalonFX(Constants.Angle.ANGLE_MOTOR_ID);

        angleMotor.setNeutralMode(NeutralMode.Brake);

        absoluteEncoder.configMagnetOffset(Constants.Angle.OFFSET);
    }

    public void SetAngle(Angle angle) {
        changeAngle = angle;
    }

    public Angle GetAngle() {
        return changeAngle;
    }

    //    public boolean isAtAngle() {
    //        return Math.abs(changeAngle.getRawAngle() - actualAngle) < deadband;
    //    }

    public void pneumatic(boolean isForward) {
        if (isForward) {
            brakeSolenoid.set(Value.kForward);
        } else {
            brakeSolenoid.set(Value.kReverse);
        }
    }

    @Override
    public void periodic() {
        /*This method will be called once per scheduler run*/
        SmartDashboard.putNumber("Arm Angle", absoluteEncoder.getAbsolutePosition());
        SmartDashboard.putString("Angle case:", " " + changeAngle);

        switch (changeAngle) {
            case ConeHighTarget: // Brings arm to cone high target
                brakeSolenoid.set(Value.kForward);
                angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                if (absoluteEncoder.getAbsolutePosition() >= Constants.Angle.CONE_HIGH_ANGLE) {
                    changeAngle =
                            Angle
                                    .Stop; /*turns motor off when it reaches the angle for the high target*/
                    angleMotor.set(ControlMode.PercentOutput, 0);
                    brakeSolenoid.set(Value.kReverse);
                }
                break;
            case ConeMiddleTarget: // Brings arm to cone middle target
                brakeSolenoid.set(Value.kForward);
                if (absoluteEncoder.getAbsolutePosition()
                        > (Constants.Angle.CONE_MIDDLE_ANGLE - 1)) {
                    if ((Constants.Angle.CONE_MIDDLE_ANGLE + 1)
                            > absoluteEncoder.getAbsolutePosition()) {
                        changeAngle = Angle.Stop;
                        brakeSolenoid.set(Value.kReverse);
                    } else {
                        angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                    }
                } else {
                    angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                }
                break;
            case CubeHighTarget: // Brings arm to the cube high target
                brakeSolenoid.set(Value.kForward);
                if (absoluteEncoder.getAbsolutePosition() > (Constants.Angle.CUBE_HIGH_ANGLE - 1)) {
                    if ((Constants.Angle.CUBE_HIGH_ANGLE + 1)
                            > absoluteEncoder.getAbsolutePosition()) {
                        changeAngle = Angle.Stop;
                        brakeSolenoid.set(Value.kReverse);
                    } else {
                        angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                    }
                } else {
                    angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                }
                break;
            case CubeMiddleTarget: // Brings arm to the cube middle target
                brakeSolenoid.set(Value.kForward);
                if (absoluteEncoder.getAbsolutePosition()
                        > (Constants.Angle.CUBE_MIDDLE_ANGLE - 1)) {
                    if ((Constants.Angle.CUBE_MIDDLE_ANGLE + 1)
                            > absoluteEncoder.getAbsolutePosition()) {
                        changeAngle = Angle.Stop;
                        brakeSolenoid.set(Value.kReverse);
                    } else {
                        angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                    }
                } else {
                    angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                }
                break;
            case Floor: // Brings arm to a position where it can pickup or dropoff pieces on the
                // floor
                brakeSolenoid.set(Value.kForward);
                if (absoluteEncoder.getAbsolutePosition() > (Constants.Angle.FLOOR_ANGLE - 1)) {
                    if ((Constants.Angle.FLOOR_ANGLE + 1) > absoluteEncoder.getAbsolutePosition()) {
                        changeAngle = Angle.Stop;
                        brakeSolenoid.set(Value.kReverse);
                    } else {
                        angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                    }
                } else {
                    angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                }
            case Down: // Brings the arm inside the robot
                brakeSolenoid.set(Value.kForward);
                angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                if (absoluteEncoder.getAbsolutePosition() <= (Constants.Angle.REST_ANGLE + 1)) {
                    changeAngle =
                            Angle
                                    .Stop; /*turns motor off when it reaches the angle for rest position*/
                    angleMotor.set(ControlMode.PercentOutput, 0);
                    brakeSolenoid.set(Value.kReverse);
                }
                break;
            case Substation: // Brings arm to a position where it can grab from a substation
                brakeSolenoid.set(Value.kForward);
                if (absoluteEncoder.getAbsolutePosition()
                        > (Constants.Angle.SUBSTATION_ANGLE - 1)) {
                    if ((Constants.Angle.SUBSTATION_ANGLE + 1)
                            > absoluteEncoder.getAbsolutePosition()) {
                        changeAngle = Angle.Stop;
                        brakeSolenoid.set(Value.kReverse);
                    } else {
                        angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_DOWN_SPEED);
                    }
                } else {
                    angleMotor.set(ControlMode.PercentOutput, Constants.Angle.ANGLE_UP_SPEED);
                }
                break;
            case Stop: // Tells arm to stop changing angle
                angleMotor.set(ControlMode.PercentOutput, 0);
                brakeSolenoid.set(Value.kReverse);
                break;
        }
    }
}
