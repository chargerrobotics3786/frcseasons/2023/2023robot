// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.networktables.GenericSubscriber;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.PIDSubsystem;
import frc.robot.Constants;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

public class ExtensionSubsystem extends PIDSubsystem {
    private ShuffleboardTab tab = Shuffleboard.getTab("Extension");
    private GenericEntry extensionkP = tab.add("Extension P", Constants.Extension.kP).getEntry();
    private GenericEntry extensionkI = tab.add("Extension I", Constants.Extension.kI).getEntry();
    private GenericEntry extensionkD = tab.add("Extension D", Constants.Extension.kD).getEntry();

    private AnalogInput potentiometerInput = new AnalogInput(2);

    private final GenericSubscriber kPSubsciber;
    private final GenericSubscriber kISubsciber;
    private final GenericSubscriber kDSubsciber;
    private final CANSparkMax extensionMotor;
    private double potentiometerReading;

    public ExtensionSubsystem() {
        super(
                new PIDController(
                        Constants.Extension.kP, Constants.Extension.kI, Constants.Extension.kD));
        kPSubsciber = extensionkP.getTopic().genericSubscribe("double");
        kISubsciber = extensionkI.getTopic().genericSubscribe("double");
        kDSubsciber = extensionkD.getTopic().genericSubscribe("double");
        extensionMotor = new CANSparkMax(Constants.Extension.EXTEND_MOTOR_ID, MotorType.kBrushless);
        extensionMotor.setIdleMode(IdleMode.kBrake);
        setSetpoint(potentiometerInput.getVoltage());
        // The voltage returned from the potentiometer tells us where the arm is extended to on the
        // robot. By setting the setpoint to the voltage when the robot is enabled, it makes sure
        // that the setpoint is where the arm already is so it shouldn't move.
        getController().setTolerance(Constants.Extension.SETPOINT_TOLERANCE);

        tab.addDouble(
                "Setpoint",
                new DoubleSupplier() {
                    @Override
                    public double getAsDouble() {
                        return getController().getSetpoint();
                    }
                });
        tab.addBoolean(
                "At Setpoint",
                new BooleanSupplier() {
                    @Override
                    public boolean getAsBoolean() {
                        return getController().atSetpoint();
                    }
                });
    }

    @Override
    public void useOutput(double output, double setpoint) {
        extensionMotor.set(output);
    }

    @Override
    public double getMeasurement() {
        return potentiometerReading;
    }

    @Override
    public void periodic() {
        super.periodic();
        potentiometerReading = potentiometerInput.getVoltage();
        double kP = kPSubsciber.getDouble(Constants.Extension.kP);
        double kI = kISubsciber.getDouble(Constants.Extension.kI);
        double kD = kDSubsciber.getDouble(Constants.Extension.kD);
        getController().setP(kP);
        getController().setI(kI);
        getController().setD(kD);
        SmartDashboard.putNumber("Potentiometer Reading", potentiometerReading);
    }
}
