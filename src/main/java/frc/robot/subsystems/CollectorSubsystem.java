// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.networktables.GenericSubscriber;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Runs the mechanism that pulls in and releases cubes and cones
 *
 * @author Kyle Briggs
 * @author Alex Danneman
 * @author Phil Lai
 */
public class CollectorSubsystem extends SubsystemBase {
    private static CollectorSubsystem instance;

    private ShuffleboardTab tab = Shuffleboard.getTab("Collector");
    private GenericEntry collectorSpeed = tab.add("Speed", Constants.Collector.SPEED).getEntry();

    private final GenericSubscriber speedSubscriber;

    private RelativeEncoder collectorEncoder;

    private CANSparkMax collectorMotor;

    private Direction collectorDirection;

    public enum Direction {
        Forward,
        Backward,
        Stop,
        IgnoreCurrentLimForward
    }

    public static CollectorSubsystem getInstance() {
        if (instance == null) {
            instance = new CollectorSubsystem();
            CommandScheduler.getInstance().registerSubsystem(instance);
        }
        return instance;
    }

    public CollectorSubsystem() {
        collectorMotor = new CANSparkMax(Constants.Collector.MOTOR_ID, MotorType.kBrushless);
        collectorDirection = Direction.Stop;
        speedSubscriber = collectorSpeed.getTopic().genericSubscribe("double");
        collectorEncoder = collectorMotor.getEncoder();
        collectorMotor.setIdleMode(IdleMode.kBrake);
        collectorMotor.setSmartCurrentLimit(Constants.Collector.MOTOR_CURRENT_LIMIT, 0, 2000);
        collectorMotor.burnFlash();
    }

    public void SetDirection(Direction direction) {
        collectorDirection = direction;
    }

    @Override
    public void periodic() {
        double speed = speedSubscriber.getDouble(Constants.Collector.SPEED);
        SmartDashboard.putNumber("CollectorCurrent", collectorMotor.getOutputCurrent());
        SmartDashboard.putNumber("Collector Speed", collectorMotor.get());
        switch (collectorDirection) {
            case Forward:
                collectorMotor.set(speed);
                if (collectorMotor.getOutputCurrent() > 17) {
                    collectorMotor.set(0);
                }
                break;
            case Backward:
                collectorMotor.set(-speed / 2);
                break;
            case Stop:
                collectorMotor.set(0);
                break;
            case IgnoreCurrentLimForward:
                collectorMotor.set(speed);
                break;
        }
    }
}
