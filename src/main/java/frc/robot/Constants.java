// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    public static class Controller {
        public static final int PRIMARY = 0;
        public static final int SECONDARY = 1;
        public static final double DEADBAND = 0.08;
    }

    public static class Angle {
        // Can id's
        public static final int CANCODER_ID = 31;
        public static final int ANGLE_MOTOR_ID = 32;
        // pneumatic hub ports
        public static final int BRAKE_OFF_PORT = 8;
        public static final int BRAKE_ON_PORT = 9;
        // offset
        public static final int OFFSET = -153;
        // angle values
        public static final double CONE_HIGH_ANGLE = 155;
        public static final double CUBE_HIGH_ANGLE = 110; // TODO
        public static final double CONE_MIDDLE_ANGLE = 135;
        public static final double CUBE_MIDDLE_ANGLE = 125; // TODO
        public static final double FLOOR_ANGLE = 60;
        public static final double REST_ANGLE = 20;
        public static final double SUBSTATION_ANGLE = 130; // estimate
        // speeds
        public static final double ANGLE_UP_SPEED = -0.2;
        public static final double ANGLE_DOWN_SPEED = 0.1;
    }

    public static class Extension {
        public static final int EXTEND_MOTOR_ID = 33;

        public static final double SETPOINT_TOLERANCE = 0.007;

        public static final double kP = 2.9;
        public static final double kI = 0;
        public static final double kD = 0;

        /** Sets if the arm is extending and which direction */
        public static enum ExtensionSetpoints {
            ConeHighTarget(0.389),
            CubeHighTarget(6.5), // TODO
            MiddleTarget(0.250),
            Substation(0.250),
            Floor(0.323),
            Retract(0.240);

            private final double setpoint;

            private ExtensionSetpoints(double setpoint) {
                this.setpoint = setpoint;
            }

            public double getSetpoint() {
                return setpoint;
            }
        }
    }

    public static class Collector {
        public static final int MOTOR_ID = 21;
        public static final double SPEED = 0.3;
        public static final int MOTOR_CURRENT_LIMIT = 10;
    }

    public static final class Swerve {
        public static final double SPEED = 10;
        public static final double MAX_VOLTAGE = 12.0;
        public static final double MAX_ACCELERATION_UNITS_PER_SEC = 2;
        public static final double MAX_ANGULAR_ACCELERATION_UNITS_PER_SEC = 2 * (Math.PI);
        public static final double MAX_ANGULAR_SPEED_RADIAN_PER_SEC = (4 * Math.PI);
        public static final double ROTATIONFRICTION_CONSTANT = -4.0;
        public static final double TRANSLATEFRICTION_CONSTANT = -1.5;
        public static final double FAST_SPEED_MODIFIER = 1.7;
        public static final double SLOW_SPEED_MODIFIER = 0.33;
    }

    public static final class Drive {
        // subtracting PI/2 to rotate it by -90 degrees
        // the cords would be swapped (x with y, x and y are both negetive)

        /*
         * TO GET THE TURNING OFFSET: rotate all of the wheels to one direction set all of the
         * turning offsets to 0, then deploy set the turning offsets to the angle values you see in
         * the drive train tab
         */

        public static final int LEFT_REAR_DRIVE_PORT = 17;
        public static final int LEFT_REAR_TURNING_PORT = 16;
        public static final int LEFT_REAR_CAN_CODER_PORT = 8;
        public static final double LEFT_REAR_TURNING_OFFSET = -Math.toRadians(320.62);

        public static final int LEFT_FRONT_DRIVE_PORT = 15;
        public static final int LEFT_FRONT_TURNING_PORT = 14;
        public static final int LEFT_FRONT_CAN_CODER_PORT = 7;
        public static final double LEFT_FRONT_TURNING_OFFSET = -Math.toRadians(254.62);

        public static final int RIGHT_FRONT_DRIVE_PORT = 13;
        public static final int RIGHT_FRONT_TURNING_PORT = 12;
        public static final int RIGHT_FRONT_CAN_CODER_PORT = 6;
        public static final double RIGHT_FRONT_TURNING_OFFSET = -Math.toRadians(74.53);

        public static final int RIGHT_REAR_DRIVE_PORT = 11;
        public static final int RIGHT_REAR_TURNING_PORT = 10;
        public static final int RIGHT_REAR_CAN_CODER_PORT = 5;
        public static final double RIGHT_REAR_TURNING_OFFSET = -Math.toRadians(38.66);

        public static final double TRACK_WIDTH =
                Units.inchesToMeters(19.5); // distance between left and right wheels
        public static final double WHEEL_BASE =
                Units.inchesToMeters(19.5); // distance between front and back wheels
        public static final SwerveDriveKinematics SWERVE_DRIVE_KINEMATICS =
                new SwerveDriveKinematics(
                        new Translation2d(-WHEEL_BASE / 2, TRACK_WIDTH / 2),
                        new Translation2d(-WHEEL_BASE / 2, -TRACK_WIDTH / 2),
                        new Translation2d(WHEEL_BASE / 2, TRACK_WIDTH / 2),
                        new Translation2d(WHEEL_BASE / 2, -TRACK_WIDTH / 2));
    }
}
