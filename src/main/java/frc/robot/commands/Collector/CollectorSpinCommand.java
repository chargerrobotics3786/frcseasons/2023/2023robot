// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.subsystems.CollectorSubsystem;

public class CollectorSpinCommand extends InstantCommand {

    private final CollectorSubsystem collectorSubsystem;
    CollectorSubsystem.Direction direction;

    /**
     * Creates a command that spins the collector at a certain speed.
     *
     * @author Kyle Briggs
     */
    public CollectorSpinCommand(
            CollectorSubsystem collectorSubsystem, CollectorSubsystem.Direction direction) {
        this.collectorSubsystem = collectorSubsystem;
        this.direction = direction;
        addRequirements(collectorSubsystem);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        collectorSubsystem.SetDirection(direction);
    }
}
