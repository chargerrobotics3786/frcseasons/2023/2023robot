package frc.robot.commands.Swerve;

import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Command that slowly spins the robot clockwise
 *
 * @author Phillip Lai
 * @author Kyle Briggs
 */
public class SwerveShowcaseCommand extends CommandBase {

    private final SwerveSubsystem swerveSubsystem;
    private final Double turningSpdFunction;
    private final SlewRateLimiter turningLimiter;

    public SwerveShowcaseCommand(SwerveSubsystem swerveSubsystem, Double turningSpdFunction) {
        this.swerveSubsystem = swerveSubsystem;
        this.turningSpdFunction = turningSpdFunction;
        addRequirements(swerveSubsystem);
        this.turningLimiter =
                new SlewRateLimiter(Constants.Swerve.MAX_ANGULAR_ACCELERATION_UNITS_PER_SEC);
    }

    @Override
    public void execute() {
        // 1. Real-time joystick inputs
        double turningSpeed = turningSpdFunction;

        // 2. Apply deadband
        turningSpeed = Math.abs(turningSpeed) > Constants.Controller.DEADBAND ? turningSpeed : 0.0;

        // 3. Make driving smoother
        turningSpeed =
                turningLimiter.calculate(turningSpeed)
                        * Constants.Swerve.MAX_ANGULAR_SPEED_RADIAN_PER_SEC;

        // 4. send the speeds off to the SwerveSubsystem to be dealt with
        swerveSubsystem.setSpeeds(0, 0, turningSpeed, false);
    }

    @Override
    public void end(boolean interrupted) {
        swerveSubsystem.stopModules();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
