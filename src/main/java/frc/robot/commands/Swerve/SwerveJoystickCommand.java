package frc.robot.commands.Swerve;

import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants;
import frc.robot.subsystems.SwerveSubsystem;
import java.util.function.Supplier;

/**
 * Creates a command that takes xbox controller joystick input to drive a swerve base
 *
 * @author Phillip Lai
 */
public class SwerveJoystickCommand extends CommandBase {

    private final SwerveSubsystem swerveSubsystem;
    private final Supplier<Double> xSpdFunction, ySpdFunction, turningSpdFunction;
    private final Supplier<Trigger> robotOrientedFunction, fastFunction, slowFunction;
    private final SlewRateLimiter xLimiter, yLimiter, turningLimiter;

    public SwerveJoystickCommand(
            SwerveSubsystem swerveSubsystem,
            Supplier<Double> xSpdFunction,
            Supplier<Double> ySpdFunction,
            Supplier<Double> turningSpdFunction,
            Supplier<Trigger> robotOrientedFunction,
            Supplier<Trigger> fastFunction,
            Supplier<Trigger> slowFunction) {
        this.swerveSubsystem = swerveSubsystem;
        this.xSpdFunction = xSpdFunction;
        this.ySpdFunction = ySpdFunction;
        this.turningSpdFunction = turningSpdFunction;
        this.robotOrientedFunction = robotOrientedFunction;
        addRequirements(swerveSubsystem);
        this.xLimiter = new SlewRateLimiter(Constants.Swerve.MAX_ACCELERATION_UNITS_PER_SEC);
        this.yLimiter = new SlewRateLimiter(Constants.Swerve.MAX_ACCELERATION_UNITS_PER_SEC);
        this.turningLimiter =
                new SlewRateLimiter(Constants.Swerve.MAX_ANGULAR_ACCELERATION_UNITS_PER_SEC);

        this.slowFunction = slowFunction;
        this.fastFunction = fastFunction;
    }

    @Override
    public void execute() {
        // 1. Real-time joystick inputs
        double xSpeed = xSpdFunction.get();
        double ySpeed = ySpdFunction.get();
        double turningSpeed = turningSpdFunction.get();

        // 2. Apply deadband
        xSpeed = Math.abs(xSpeed) > Constants.Controller.DEADBAND ? xSpeed : 0.0;
        ySpeed = Math.abs(ySpeed) > Constants.Controller.DEADBAND ? ySpeed : 0.0;
        turningSpeed = Math.abs(turningSpeed) > Constants.Controller.DEADBAND ? turningSpeed : 0.0;

        // 3. Make driving smoother
        if (slowFunction.get().getAsBoolean()) {
            xSpeed =
                    xLimiter.calculate(xSpeed)
                            * (Constants.Swerve.SPEED * Constants.Swerve.SLOW_SPEED_MODIFIER);
            ySpeed =
                    yLimiter.calculate(ySpeed)
                            * (Constants.Swerve.SPEED * Constants.Swerve.SLOW_SPEED_MODIFIER);
            turningSpeed =
                    turningLimiter.calculate(turningSpeed)
                            * Constants.Swerve.MAX_ANGULAR_SPEED_RADIAN_PER_SEC;
        } else if (fastFunction.get().getAsBoolean()) {
            xSpeed =
                    xLimiter.calculate(xSpeed)
                            * (Constants.Swerve.SPEED * Constants.Swerve.FAST_SPEED_MODIFIER);
            ySpeed =
                    yLimiter.calculate(ySpeed)
                            * (Constants.Swerve.SPEED * Constants.Swerve.FAST_SPEED_MODIFIER);
            turningSpeed =
                    turningLimiter.calculate(turningSpeed)
                            * Constants.Swerve.MAX_ANGULAR_SPEED_RADIAN_PER_SEC;
        } else {
            xSpeed = xLimiter.calculate(xSpeed) * Constants.Swerve.SPEED;
            ySpeed = yLimiter.calculate(ySpeed) * Constants.Swerve.SPEED;
            turningSpeed =
                    turningLimiter.calculate(turningSpeed)
                            * Constants.Swerve.MAX_ANGULAR_SPEED_RADIAN_PER_SEC;
        }

        // 4. send the speeds off to the SwerveSubsystem to be dealt with

        swerveSubsystem.setSpeeds(
                xSpeed, ySpeed, turningSpeed, robotOrientedFunction.get().getAsBoolean());
    }

    @Override
    public void end(boolean interrupted) {
        swerveSubsystem.stopModules();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
