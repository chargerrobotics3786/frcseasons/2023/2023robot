package frc.robot.commands.Swerve;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Command that resets the navX to zero
 *
 * @author Kyle Briggs
 */
public class GyroZeroCommand extends CommandBase {

    private final SwerveSubsystem swerveSubsystem;

    public GyroZeroCommand(SwerveSubsystem swerveSubsystem) {
        this.swerveSubsystem = swerveSubsystem;
        addRequirements(swerveSubsystem);
    }

    @Override
    public void execute() {
        swerveSubsystem.zeroGyro();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
