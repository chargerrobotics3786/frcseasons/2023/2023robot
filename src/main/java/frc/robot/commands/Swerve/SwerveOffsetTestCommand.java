package frc.robot.commands.Swerve;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * Command that slowly spins the robot forward. Used to test if offsets are accurates
 *
 * @author Phillip Lai
 * @author Kyle Briggs
 */
public class SwerveOffsetTestCommand extends CommandBase {

    private final SwerveSubsystem swerveSubsystem;
    double xSpeed;
    double ySpeed;

    public SwerveOffsetTestCommand(SwerveSubsystem swerveSubsystem, double xSpeed, double ySpeed) {
        this.swerveSubsystem = swerveSubsystem;
        addRequirements(swerveSubsystem);

        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void execute() {
        swerveSubsystem.setSpeeds(xSpeed, ySpeed, 0, false);
    }

    @Override
    public void end(boolean interrupted) {
        swerveSubsystem.stopModules();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
