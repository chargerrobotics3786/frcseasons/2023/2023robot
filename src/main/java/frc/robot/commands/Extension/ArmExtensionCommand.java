// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Extension;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.robot.Constants;
import frc.robot.subsystems.ExtensionSubsystem;

public class ArmExtensionCommand extends InstantCommand {
    private final ExtensionSubsystem extensionSubsytem;
    private Constants.Extension.ExtensionSetpoints extension;
    /**
     * Creates a command that extends the arm.
     *
     * @author Kyle Briggs
     */
    public ArmExtensionCommand(
            ExtensionSubsystem extensionSubsystem,
            Constants.Extension.ExtensionSetpoints extension) {
        this.extensionSubsytem = extensionSubsystem;
        this.extension = extension;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(extensionSubsytem);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void initialize() {
        extensionSubsytem.setSetpoint(extension.getSetpoint());
    }

    @Override
    public boolean isFinished() {
        return extensionSubsytem.getController().atSetpoint();
    }
}
