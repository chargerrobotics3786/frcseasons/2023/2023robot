// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Angle;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AngleSubsystem;

public class PneumaticTestCommand extends CommandBase {
    /** Creates a new PneumaticTestCommand. */
    AngleSubsystem angleSubsystem;

    Boolean isForward;

    public PneumaticTestCommand(AngleSubsystem angleSubsystem, boolean isForward) {
        this.angleSubsystem = angleSubsystem;
        this.isForward = isForward;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(angleSubsystem);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        angleSubsystem.pneumatic(isForward);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
