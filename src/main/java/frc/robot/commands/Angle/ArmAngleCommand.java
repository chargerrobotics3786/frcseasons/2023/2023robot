// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Angle;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.AngleSubsystem.Angle;

public class ArmAngleCommand extends CommandBase {
    private final AngleSubsystem angleSubsystem;
    private Angle angle;
    /**
     * Creates a command that changes the angle of the motor.
     *
     * @author Kyle Briggs
     */
    public ArmAngleCommand(AngleSubsystem angleSubsystem, Angle angle) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.angleSubsystem = angleSubsystem;
        this.angle = angle;
        addRequirements(angleSubsystem);
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void initialize() {
        angleSubsystem.SetAngle(angle);
    }

    @Override
    public boolean isFinished() {
        if (angleSubsystem.GetAngle() == Angle.Stop) {
            return true;
        } else {
            return false;
        }
    }
}
