// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Auto;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.SwerveSubsystem;

public class AutoDriveCommandGroup extends SequentialCommandGroup {
    public AutoDriveCommandGroup(SwerveSubsystem swerveSubsystem) {
        addCommands(
                new SwerveAutoDrive(swerveSubsystem, 0, -2),
                new WaitCommand(3),
                new SwerveAutoDrive(swerveSubsystem, 0, 0));
    }
}
