// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;

public class SwerveAutoDrive extends CommandBase {
    /** Creates a new SwerveAutoDrive. */
    SwerveSubsystem swerveSubsystem;

    double xSpeed;
    double ySpeed;

    public SwerveAutoDrive(SwerveSubsystem swerveSubsystem, double xSpeed, double ySpeed) {
        // Use addRequirements() here to declare subsystem dependencies.
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.swerveSubsystem = swerveSubsystem;
        addRequirements(swerveSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {}

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        swerveSubsystem.setSpeeds(xSpeed, ySpeed, 0, false);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return true;
    }
}
