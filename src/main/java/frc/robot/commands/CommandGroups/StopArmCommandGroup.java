// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.Angle.ArmAngleCommand;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.AngleSubsystem.Angle;
import frc.robot.subsystems.ExtensionSubsystem;

public class StopArmCommandGroup extends SequentialCommandGroup {
    public StopArmCommandGroup(
            AngleSubsystem angleSubsystem, ExtensionSubsystem extensionSubsystem) {
        addCommands(
                new ArmAngleCommand(angleSubsystem, Angle.Stop)
                        .beforeStarting(
                                () -> {
                                    extensionSubsystem.setSetpoint(
                                            extensionSubsystem.getMeasurement());
                                },
                                extensionSubsystem));
    }
}
