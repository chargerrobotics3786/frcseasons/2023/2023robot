// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.CommandGroups;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.Angle.ArmAngleCommand;
import frc.robot.commands.Extension.ArmExtensionCommand;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.AngleSubsystem.Angle;
import frc.robot.subsystems.ExtensionSubsystem;

public class AngleExtendCommandGroup extends SequentialCommandGroup {
    public AngleExtendCommandGroup(
            AngleSubsystem angleSubsystem,
            ExtensionSubsystem extensionSubsystem,
            Angle angle,
            Constants.Extension.ExtensionSetpoints extension) {
        addCommands(
                new ArmAngleCommand(angleSubsystem, angle),
                new ArmExtensionCommand(extensionSubsystem, extension));
    }
}
