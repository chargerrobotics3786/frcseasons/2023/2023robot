// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.Constants.Extension.ExtensionSetpoints;
import frc.robot.commands.Angle.ArmAngleCommand;
import frc.robot.commands.Angle.PneumaticTestCommand;
import frc.robot.commands.Auto.AutoBalanceCommandGroup;
import frc.robot.commands.Auto.AutoDriveCommandGroup;
import frc.robot.commands.Auto.AutoScoreDriveOutCommandGroup;
import frc.robot.commands.Auto.AutoStopCommandGroup;
import frc.robot.commands.Collector.CollectorSpinCommand;
import frc.robot.commands.CommandGroups.AngleExtendCommandGroup;
import frc.robot.commands.CommandGroups.FloorCommandGroup;
import frc.robot.commands.CommandGroups.RetractCommandGroup;
import frc.robot.commands.CommandGroups.StopArmCommandGroup;
import frc.robot.commands.Extension.ArmExtensionCommand;
import frc.robot.commands.Swerve.GyroZeroCommand;
import frc.robot.commands.Swerve.SwerveJoystickCommand;
import frc.robot.commands.Swerve.SwerveOffsetTestCommand;
import frc.robot.commands.Swerve.SwerveShowcaseCommand;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.AngleSubsystem.Angle;
import frc.robot.subsystems.CollectorSubsystem;
import frc.robot.subsystems.CollectorSubsystem.Direction;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.SwerveSubsystem;

public class RobotContainer {
    // Subsystems
    private final SwerveSubsystem swerveSubsystem = new SwerveSubsystem();
    private AngleSubsystem angleSubsystem = new AngleSubsystem();
    private ExtensionSubsystem extensionSubsystem = new ExtensionSubsystem();
    private final CollectorSubsystem collectorSubsystem = new CollectorSubsystem();

    // Command Groups
    private AngleExtendCommandGroup coneHighTargetCommand =
            new AngleExtendCommandGroup(
                    angleSubsystem,
                    extensionSubsystem,
                    Angle.ConeHighTarget,
                    ExtensionSetpoints.ConeHighTarget);
    private AngleExtendCommandGroup coneMiddleTargetCommand =
            new AngleExtendCommandGroup(
                    angleSubsystem,
                    extensionSubsystem,
                    Angle.ConeMiddleTarget,
                    ExtensionSetpoints.MiddleTarget);
    private AngleExtendCommandGroup cubeHighTargetCommand =
            new AngleExtendCommandGroup(
                    angleSubsystem,
                    extensionSubsystem,
                    Angle.CubeHighTarget,
                    ExtensionSetpoints.CubeHighTarget);
    private AngleExtendCommandGroup cubeMiddleTargetCommand =
            new AngleExtendCommandGroup(
                    angleSubsystem,
                    extensionSubsystem,
                    Angle.CubeMiddleTarget,
                    ExtensionSetpoints.MiddleTarget);
    private FloorCommandGroup floorCommand =
            new FloorCommandGroup(angleSubsystem, extensionSubsystem);
    private AngleExtendCommandGroup substationCommand =
            new AngleExtendCommandGroup(
                    angleSubsystem,
                    extensionSubsystem,
                    Angle.Substation,
                    ExtensionSetpoints.Substation);
    /** Command group that retracts the arm to be fully inside the robot */
    private RetractCommandGroup retractCommandGroup =
            new RetractCommandGroup(angleSubsystem, extensionSubsystem);
    /** Command group that stops the arm wherever it is */
    private StopArmCommandGroup stopArmCommandGroup =
            new StopArmCommandGroup(angleSubsystem, extensionSubsystem);
    // Swerve
    /** Command that slowly spins the robot clockwise */
    private final SwerveShowcaseCommand showcaseCommand =
            new SwerveShowcaseCommand(swerveSubsystem, 0.25);
    private final SwerveOffsetTestCommand forwardOffsetTestCommand =
            new SwerveOffsetTestCommand(swerveSubsystem, 0, 1);
    private final SwerveOffsetTestCommand sidewaysOffsetTestCommand =
            new SwerveOffsetTestCommand(swerveSubsystem, 1, 0);
    /**Command that resets the navX and sets the direction the robot is currently facing to zero */
    private final GyroZeroCommand gyroZeroCommand = new GyroZeroCommand(swerveSubsystem);

    // Extension
    /**
     * Command that extends the arm to the furthest the extension can go, and raises it to be
     * parallel with the ground
     */
    private final ArmExtensionCommand extendCommand =
            new ArmExtensionCommand(extensionSubsystem, ExtensionSetpoints.ConeHighTarget);
    /** Command that retracts the extension part of the arm to be fully retracted */
    private final ArmExtensionCommand retractCommand =
            new ArmExtensionCommand(extensionSubsystem, ExtensionSetpoints.Retract);

    private final ArmExtensionCommand floorExtendCommand =
            new ArmExtensionCommand(extensionSubsystem, ExtensionSetpoints.Floor);

    // Angle
    private final ArmAngleCommand ConeHighCommand =
            new ArmAngleCommand(angleSubsystem, Angle.ConeHighTarget);
    private final ArmAngleCommand FloorCommand = new ArmAngleCommand(angleSubsystem, Angle.Floor);
    private final ArmAngleCommand downAngle = new ArmAngleCommand(angleSubsystem, Angle.Down);
    private final ArmAngleCommand CubeMiddleCommand =
            new ArmAngleCommand(angleSubsystem, Angle.CubeMiddleTarget);
    private final ArmAngleCommand SubstationCommand =
            new ArmAngleCommand(angleSubsystem, Angle.Substation);

    // Pneumatics
    private final PneumaticTestCommand brakeOnCommand =
            new PneumaticTestCommand(angleSubsystem, true);
    private final PneumaticTestCommand brakeOffCommand =
            new PneumaticTestCommand(angleSubsystem, false);

    // Collector
    private final CollectorSpinCommand collectorForwardCommand =
            new CollectorSpinCommand(collectorSubsystem, Direction.Forward);
    private final CollectorSpinCommand collectorBackwardCommand =
            new CollectorSpinCommand(collectorSubsystem, Direction.Backward);
    private final CollectorSpinCommand collectorOffCommand =
            new CollectorSpinCommand(collectorSubsystem, Direction.Stop);
    private final CollectorSpinCommand collectorIgnoreCurrentLimitForwardCommand =
            new CollectorSpinCommand(collectorSubsystem, Direction.IgnoreCurrentLimForward);

    // Auto
    private AutoDriveCommandGroup autoDrive = new AutoDriveCommandGroup(swerveSubsystem);
    private AutoBalanceCommandGroup autoBalance = new AutoBalanceCommandGroup(swerveSubsystem);
    private AutoScoreDriveOutCommandGroup autoScoreDriveOut =
            new AutoScoreDriveOutCommandGroup(swerveSubsystem);
    private AutoStopCommandGroup autoStop = new AutoStopCommandGroup(swerveSubsystem);

    // Sendable Chooser
    private SendableChooser<Integer> autoChooser = new SendableChooser<>();

    // Xbox Controllers
    private final CommandXboxController primary =
            new CommandXboxController(Constants.Controller.PRIMARY);
    private final CommandXboxController secondary =
            new CommandXboxController(Constants.Controller.SECONDARY);

    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer() {
        swerveSubsystem.setDefaultCommand(
                new SwerveJoystickCommand(
                        swerveSubsystem,
                        () -> -primary.getLeftX(), // X Speed
                        () -> primary.getLeftY(), // Y Speed
                        () -> primary.getRightX(), // Turning Speed
                        () -> primary.leftBumper(), // Robot Oriented
                        () -> primary.leftTrigger(), // Fast Mode
                        () -> primary.rightTrigger())); // Slow Mode
        autoChooser.addOption("Balance", 0);
        autoChooser.addOption("Drive Out", 1);
        autoChooser.addOption("Score, Drive Out", 2);
        autoChooser.addOption("Stay In Place", 3);
        SmartDashboard.putData(autoChooser);
    }

    // Sets different button bindings for test mode
    public void initBindings(boolean isTest) {
        unbindBindings();
        if (isTest) {
            configureTestBindings();
        } else {
            configureBindings();
        }
    }

    private void configureBindings() {
        // Swerve
        primary.a().toggleOnTrue(showcaseCommand);
        // Arm Command Groups
        secondary.a().toggleOnTrue(retractCommandGroup);
        secondary.b().toggleOnTrue(stopArmCommandGroup);
        secondary.x().toggleOnTrue(floorCommand);
        secondary.y().toggleOnTrue(substationCommand);
        secondary.povUp().toggleOnTrue(coneHighTargetCommand);
        secondary.povRight().toggleOnTrue(coneMiddleTargetCommand);
        secondary.povDown().toggleOnTrue(cubeMiddleTargetCommand);
        // secondary.povLeft().toggleOnTrue(cubeHighTargetCommand);
        // TODO ^
        // Collector
        secondary.rightBumper().toggleOnTrue(collectorForwardCommand);
        secondary.leftBumper().toggleOnTrue(collectorBackwardCommand);
        secondary.leftTrigger().toggleOnTrue(collectorOffCommand);
        secondary.rightTrigger().toggleOnTrue(collectorIgnoreCurrentLimitForwardCommand);
        // Extension
        secondary.start().toggleOnTrue(retractCommand);
    }

    private void configureTestBindings() {
        // Swerve
        primary.povRight().toggleOnTrue(sidewaysOffsetTestCommand);
        primary.povLeft().toggleOnTrue(forwardOffsetTestCommand);
        primary.start().toggleOnTrue(gyroZeroCommand);
        // Extension
        secondary.a().toggleOnTrue(extendCommand);
        secondary.b().toggleOnTrue(retractCommand);
        secondary.start().toggleOnTrue(floorExtendCommand);
        // Arm Command Groups
        secondary.leftBumper().toggleOnTrue(coneHighTargetCommand);
        secondary.povUp().toggleOnTrue(coneMiddleTargetCommand);
        secondary.leftTrigger().toggleOnTrue(retractCommandGroup);
        secondary.rightBumper().toggleOnTrue(stopArmCommandGroup);
        // Angle
        secondary.rightTrigger().toggleOnTrue(downAngle);
        secondary.x().toggleOnTrue(FloorCommand);
        secondary.y().toggleOnTrue(ConeHighCommand);
        secondary.povLeft().toggleOnTrue(CubeMiddleCommand);
        secondary.povRight().toggleOnTrue(SubstationCommand);
        // Pneumatic
        primary.povUp().toggleOnTrue(brakeOnCommand);
        primary.povDown().toggleOnTrue(brakeOffCommand);
        // Collector
        primary.a().toggleOnTrue(collectorIgnoreCurrentLimitForwardCommand);
        primary.b().toggleOnTrue(collectorBackwardCommand);
        primary.x().toggleOnTrue(collectorOffCommand);
    }

    /** clears all controller bindings */
    private void unbindBindings() {
        CommandScheduler.getInstance().getActiveButtonLoop().clear();
    }

    public void enableSubsystems() {
        extensionSubsystem.enable();
    }

    public void disableSubsystems() {
        extensionSubsystem.disable();
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() { // should be Command type method
        Integer path;
        if (autoChooser != null) {
            path = autoChooser.getSelected();
        } else {
            return null;
        }

        switch (path) {
            case (0):
                return autoBalance;
            case (1):
                return autoDrive;
            case (2):
                return autoScoreDriveOut;
            case (3):
                return autoStop;
            default:
                return null;
        }
    }
}
